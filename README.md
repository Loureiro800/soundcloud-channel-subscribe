# AllTrapMusic Download Pages

They are not difficult just have loads of steps - with practice you will start doing this in 10/15min


How to make the download page using the template
======
- Replace the images inside the folder "img" named 'artwork_dnf.jpg' and 'facebook.jpg' with new artwork
- Create a zip inside the project with the mp3/mp4 to be used in the download (don't use CAPs or spaces)
- Edit the index.html
	* Change the [title], [facebookmeta], [div.info] and [div.note] with the information needed.
- Login on [Soundcloud Apps](http://soundcloud.com/you/apps)
	* Click on 'Register a new application'
	* Name the appliction like "All Trap Music | [artist] - [track]"
	* Add the link of the download page you building - 'http://alltrapmusic.com/downloads/[downloadpage]/' to 'Website of your app' and 'Redirect URI for Authentication'
	* Copy the [client ID]
	* Save app
- Edit the index.html
	* Edit the client_id: '[old client ID]' with new [client ID]
	* Edit the redirect_uri: '[old link]' with 'http://alltrapmusic.com/downloads/[downloadpage]/' used while building soundcloud app
- If they send Soundclouds to follow like for example [https://soundcloud.com/cameronmakesnoise] this are the steps you need to do
	* open link of artist e.g. https://soundcloud.com/cameronmakesnoise
	* View page source and search for content="soundcloud://users:[code]" (remove [code] while searching)
	* Copy the [code]
- Edit the index.html
	* replace 
		- SC.put('/me/followings/[old-code]'); 
		- with 
		- SC.put('/me/followings/[code]'); 
- Upload the zip with track in the download page 
	* http://alltrapmusic.com/downloads/[downloadpage]/[track.zip]
- Open [found](http://found.ee/)
	* Login
		- User: AEI-UKF
		- Pass: remarketing
	* Main Navigation ] Users ] Manage Users ] Search for AllTrapMusic and click 'Switch'
	* In the Shorten add the link to the zip file "http://alltrapmusic.com/downloads/[downloadpage]/[track.zip]"
	* Main Navigation ] links ] Copy Smart URL
- Edit the index.html
	* Search for: 'a href="http://found.ee/"'
	* Replace the old Smart URL and replace with new one
	* Search for the link right after that 'a href="https://mega.nz/"' and you can just use a direct URL to the zip file e.g. "http://alltrapmusic.com/downloads/[downloadpage]/[track.zip]"
- Add facebook meta
- DONE!! 


